const hello = () => alert("Hello world!");

const aVeryLongFunction = () => {
    let a = 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    a += 1;
    return a;
}

export default hello;
